import { defineStore } from 'pinia';
import BigNumber from "bignumber.js";

export const useAirplaneStore = defineStore("AirplaneStore", {
    state: () => {
        return {
            dinero: new BigNumber(0)
        }
    },


    // getters
    getters: {
        money: (state) => state.dinero.toString(),
    },


    // actions
    actions: {
        increment() {
            this.dinero = BigNumber.sum(this.dinero, 1);
        },

        incrementBy(amount) {
            this.dinero = BigNumber.sum(this.dinero, amount);
        }
    }
});
