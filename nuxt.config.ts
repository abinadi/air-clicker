// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    pages: true,
    modules: [
        '@nuxtjs/tailwindcss',
        '@pinia/nuxt'
    ]
})
